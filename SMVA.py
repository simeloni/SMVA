#Import quite a few tools
import ROOT
import sys
import time
from ROOT import TFile
import root_numpy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import pandas as pd
import pandas.core.common as com
from pandas.core.index import Index
from pandas.tools import plotting
from pandas.tools.plotting import scatter_matrix
from root_numpy import root2array, root2rec, tree2rec, array2root, rec2array, tree2array
from root_numpy.testdata import get_filepath
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import classification_report, roc_auc_score
from sklearn.cross_validation import train_test_split
from sklearn.metrics import roc_curve, auc
from sklearn import preprocessing
from array import array

from script_functions2 import *
from inputfile import *



start_time = time.time()


#-----------Control the input options-------------------------------#
control_inputs(sys.argv)
 
#-------------Apply the preliminary selections on the samples-------#
sig_preselections_tt = '&&'.join(sig_preselections_tt)
bkg_preselections_tt = '&&'.join(bkg_preselections_tt)

sig_preselections = '&&'.join(sig_preselections)
bkg_preselections = '&&'.join(bkg_preselections)

if ('--NotRecreate' in sys.argv):
	print 'Not recreating the reduced rate file: input taken from ' 
	print '--SIGNAL :    ' + output_sigfilename_cuts
	print '--BACKGROUND: ' +  output_bkgfilename_cuts
	print '\n'
    
	selection_time = time.time()


else :
	print 'Applying the preliminary selections'
	
	
	reduce_rate(input_sigfilename, output_sigfilename_cuts, input_sigtreename, output_sigtreename, sig_preselections, N_events_signal, sig_truth_matching)
	reduce_rate(input_bkgfilename, output_bkgfilename_cuts, input_bkgtreename, output_bkgtreename,  bkg_preselections, N_events_background, bkg_truth_matching)

	

	selection_time = time.time()
	print ('----time : %s seconds \n' % (selection_time - start_time))
	

#------------------create the train and test sample-----------------#

print 'Creating the test and train sample'

sig_tt_file = TFile(output_sigfilename_cuts)
bkg_tt_file = TFile(output_bkgfilename_cuts)

sig_tt_tree = sig_tt_file.Get(output_sigtreename)
bkg_tt_tree = bkg_tt_file.Get(output_bkgtreename)


sig_tt_rec = tree2rec(sig_tt_tree, branches, sig_preselections_tt)
bkg_tt_rec = tree2rec(bkg_tt_tree, branches, bkg_preselections_tt)

sig_tt_array = rec2array(sig_tt_rec)
bkg_tt_array = rec2array(bkg_tt_rec)


if ('--StandardizeData' in sys.argv):
	#standardize the dataset
	sig_tt_array = preprocessing.scale(sig_tt_array)
	bkg_tt_array = preprocessing.scale(bkg_tt_array)

X_tt = np.concatenate((sig_tt_array, bkg_tt_array))
y_tt = np.concatenate((np.ones(sig_tt_array.shape[0]), np.zeros(bkg_tt_array.shape[0])))




#split the sample for training and testing
X_train, X_test, y_train, y_test = train_test_split(X_tt, y_tt, test_size = Train_Ratio, random_state = 42)

creating_test_time = time.time()
print ('----time : %s seconds \n' % (creating_test_time - selection_time ))

#----------------------create the apply MVA sample------------------#

print 'Create the sample on which the bdt will run'

sig_app_file = TFile(output_sigfilename_cuts)
sig_app_tree = sig_app_file.Get(output_sigtreename)

bkg_app_file = TFile(output_bkgfilename_cuts)
bkg_app_tree = bkg_app_file.Get(output_bkgtreename)

sig_app_rec = tree2rec(sig_app_tree)
bkg_app_rec = tree2rec(bkg_app_tree)

sig_app_array = rec2array(sig_app_rec, branches)
bkg_app_array = rec2array(bkg_app_rec, branches)

if ('--StandardizeData' in sys.argv):
	sig_app_array = preprocessing.scale(sig_app_array)
	bkg_app_array = preprocessing.scale(bkg_app_array)

creating_apply_time = time.time()
print ('----time : %s seconds \n' % (creating_apply_time - creating_test_time ))

#----------------------Define And Train The MVA---------------------#


print 'Training The MVA'

bdt.fit(X_train, y_train)

train_time = time.time()
print ('----time : %s seconds \n' % (train_time - creating_apply_time))

#---------------------Make the ROC Curve for the BDT----------------#

print 'Applying to the test sample'
decisions_test = bdt.predict_proba(X_test)[:,1]
#decisions_test = bdt.decision_function(X_test)

apply_test_time = time.time()

print ('----time : %s seconds \n' % (apply_test_time - train_time))

if ('--MakeROC' in sys.argv):
	print 'Printing the ROC curve of the MVA'
	fpr, tpr, thresholds = roc_curve(y_test, decisions_test)

	make_ROC_curve (fpr, tpr, thresholds, output_plots_folder+'ROC.pdf')

	roc_time = time.time()
	print ('----time : %f seconds \n' % float(roc_time - apply_test_time))

else:
	roc_time = time.time()

#----------Compare the performances of more than 1 MVA-------------#

if ('--ComparePerformances' in sys.argv):
	print 'Comparing the performances of the given MVAs'
	compare_ROC_curves (X_train, y_train, X_test, y_test, MVAS_list, MVAS_labels, output_plots_folder+'compare_performances.pdf')

	performances_time = time.time()
	print ('----time : %s seconds \n' % (performances_time - roc_time))

else: 
	performances_time = time.time()
	


#----------Compare response on Train and Test Sample----------------#
if ('--CompareTrainTest' in sys.argv):
	print 'Comparing Train and Test Sample'

	compare_train_test(bdt, X_train, y_train, X_test, y_test, 30,output_plots_folder+'compare_response.pdf', 'predict_proba')
	#compare_train_test(bdt, X_train, y_train, X_test, y_test, 30,output_plots_folder+'compare_response.pdf', 'decision_function')
	
	compare_time = time.time()
	print ('----time : %s seconds \n' % (compare_time - performances_time))

else:
	compare_time = time.time()

#-----------Make the correlation matrix-----------------------------#
if ('--CorrelationMatrix' in sys.argv):
	print 'Creating the Correlation Matrix'
	

	correlation_time = time.time()
	print ('----time : %s seconds \n' % (correlation_time - compare_time))
	
	
	correlation_matrix (X_train, y_train, branches, output_plots_folder+'correlation_matrix.pdf')

else:
	correlation_time = time.time()
#----------------Apply the BDT to the original sample---------------#

print 'Applying the MVA to the sample'

decision_sig = bdt.predict_proba(sig_app_array)[:,1]
decision_bkg = bdt.predict_proba(bkg_app_array)[:,1]


#decision_sig = bdt.decision_function(sig_app_array)
#decision_bkg = bdt.decision_function(bkg_app_array)

apply_time = time.time()
print ('----time : %s seconds \n' % (apply_time - correlation_time))

#--------Save the output in a new Branch and write a new File-------#

print 'Saving the output to the new Files'

sig_out_file = TFile(output_sigfilename_cuts_bdt, 'UPDATE')
sig_out_tree = TTree(output_sigtreename, output_sigtreename)


sig_out_tree = sig_app_tree.CloneTree()

#fill the Signal Branch
sig_var = array('f', [0]) #temp variable

new_sig_branch = sig_out_tree.Branch(mva_branch_name, sig_var, mva_branch_name+'/F')

for i in decision_sig:
	sig_var[0] = i
	new_sig_branch.Fill()

#writing the new File
sig_out_file.cd()
sig_out_tree.Write("", ROOT.TObject.kOverwrite)



bkg_out_file = TFile(output_bkgfilename_cuts_bdt, 'UPDATE')
bkg_out_tree = TTree(output_bkgtreename, output_bkgtreename)

bkg_out_tree = bkg_app_tree.CloneTree()

#Filling the Background Branch
bkg_var = array('f', [0]) #temp variable

new_bkg_branch = bkg_out_tree.Branch(mva_branch_name, bkg_var, mva_branch_name+'/F')

for k in decision_bkg:
	bkg_var[0] = k
	new_bkg_branch.Fill()

#writing the new File
bkg_out_file.cd()
bkg_out_tree.Write("", ROOT.TObject.kOverwrite)

save_time = time.time()
print ('----time : %s seconds \n' % (save_time - apply_time))


#-----------Evaluating the Efficiency and the Retention of the MVA-------------#

if ('--MakePlots' in sys.argv):
	print 'EValuating the efficiency and the retention of the MVA'

	y_app = np.concatenate((np.ones(decision_sig.shape[0]), np.zeros(decision_bkg.shape[0])))

	X_app = np.concatenate((decision_sig, decision_bkg))

	retention,efficiency, thr = roc_curve (y_app, X_app)
	
	bkg_entries = bkg_out_tree.GetEntries()

	for j in range (0, retention.shape[0]):
		#retention[j] = 1. - retention[j] 
		retention[j] = retention[j] * bkg_entries / N_events_background * background_rate * event_size
		j += 1

	efficiency_time = time.time()
	print ('----time : %s seconds \n' % (efficiency_time - save_time))

	#--------producing and saving Plots-----------------------#
	print 'Producing and saving Plots'

	print_plot(thr, efficiency, output_plots_folder 
	+'efficiency.pdf', 'threshold_value', 'signal efficiency', 'SIGNAL EFFICIENCY')
	print_plot(thr, retention,  output_plots_folder 
	+'retention.pdf',
	'threshold_value', 'retention [GB/s]', 'BACKGROUND RETENTION vs THRESHOLD VALUES')
	print_plot(retention, efficiency,  output_plots_folder 
	+'eff_vs_ret.pdf',
	'retention [GB/s]', 'signal efficiency', 'SIGNAL EFFICIENCY vs BACKGROUND RETENTION')

	plot_time = time.time()
	print ('----time : %s seconds \n' % (plot_time - efficiency_time))

