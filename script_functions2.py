import ROOT
from ROOT import TFile, TTree
import math
import numpy as np
import sklearn
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import sys
import time
import pandas as pd
import pandas.core.common as com
from pandas.core.index import Index
from pandas.tools import plotting
from pandas.tools.plotting import scatter_matrix

from sklearn.metrics import roc_curve, auc

def binomialError(numerator, denominator):
	""" 	Function that compute the binomial efficiency error 
		given the numerator and the denominator of the 
		efficiency. If the numerator is greater than the 
		denominator the fucntion returns 0
		
		input:		*) numerator[float]
				*) denominator[float]
						"""
	if (numerator > denominator):
		print "WARNING: numerator is greater than the denominator in efficiency evaluation. Error is set to 0."
		return 0
	else:
		efficiency = float(numerator) / float(denominator)
		return math.sqrt(efficiency * (1 - efficiency) / denominator)	    
	

def reduce_rate(input_filename, output_filename, input_treename, output_treename, selections, original_number = 0, pre_selection=''):
	
	"""     Function that takes in input a ROOT file called 
		input_filename and creates another ROOT file 
		called output_filename after having applied the 
		selections to the Branches of the TTree 
		called treename. Computes the efficiency of the selections with
		respect to the original number of events, with respect to the 
		number of events inside the input_file, and with respect to the 
		number of events that pass the pre_selection. In the output 
		the preselection is assumed to be the truth matching selection
	
		 input: 	*) input_filename['string'] 
				*) output_filename['string']
				*) input_treename['string']
				*) output_treename['string']
				*) output_treename['string']
				*) selections['string']
				*) original_number[int]
				*) pre_selection['string']
							"""
	print "==============================================================="
	print '\n'
	
	print "INFO: Reducing File                          ", input_filename
	input_file  = TFile.Open(input_filename) 
	print "INFO: Saving File after the selections       ", output_filename 
	output_file = TFile(output_filename, 'RECREATE')	

	print "INFO: Loading Tree                           ", input_treename
	old_tree = input_file.Get(input_treename)
	print "INFO: Saving Tree after the selections 	     ", output_treename
	print "Applying selections..."	
	
	new_tree = old_tree.CopyTree(selections)
	#new_tree.SetNameTitle(output_treename, output_treename)
		
	new_tree.Write()

	num_events         = old_tree.GetEntries()
	num_sel_events     = new_tree.GetEntries()
	if (original_number != 0):
		num_pre_events     = old_tree.GetEntries(pre_selection)
		num_sel_pre_events = new_tree.GetEntries(pre_selection)
	
	
		
	print "---------------------------------------------------------------"
	if (original_number != 0):
		print "INFO: Total events processed                              ", original_number
	print "INFO: Total candidates                                    ", num_events
	
	if(pre_selection != ""):
		print "INFO: Candidates passing Truth Matching                   ", num_pre_events
	print "INFO: Candidates passing Preselections                    ", num_sel_events
	
	if (pre_selection != ""): 
		print "INFO: Candidates passing Preselections and Truth Matching ", num_sel_pre_events 
		
	print "\n"
	print "---------------------------------------------------------------"
	if (original_number != 0):
		print "OUTPUT: Selected Candidates per Event                     ", float(num_sel_events) / original_number, " +/- ", binomialError(num_sel_events, original_number)
	print "OUTPUT: Selection Efficiency                              ", float(num_sel_events) / num_events, " +/- ", binomialError(num_sel_events, num_events)
	if (pre_selection != ""):
		print "OUTPUT: Selection Efficiency with            		 " 
		print "        respect to Truth Matched Candidates               ", float(num_sel_pre_events) / num_pre_events, " +/- ", binomialError(num_sel_pre_events, num_pre_events)
	print "---------------------------------------------------------------"
	print "==============================================================="
	print '\n'
			


def make_ROC_curve (fpr, tpr, thresholds, output_name = 'ROC.pdf'):
	""" Function that makes a plot of the ROC function given the 
		false positive rate array and the true positive rate array	 
		and creates a file with the given name

		  input: 	 *)fpr['array'] = false positive rates
				 *)tpr['array'] = true positive rates
				 *)thresholds['array'] = thresholds values
				 *)output_name['string'] = name and path of the output
				 *)auc['float'] = area under the ROC curve
	"""
	roc_auc = auc(fpr, tpr)
	plt.plot(fpr, tpr, lw=1, label='ROC (area = %0.2f)'%(roc_auc))
	plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
	plt.xlim([-0.05, 1.05])
	plt.ylim([-0.05, 1.05])
	plt.xlabel('False Positive Rate', ha='right', x=1)
	plt.ylabel('True Positive Rate', ha='right', y=1)
	plt.title('Receiver operating characteristic')
	plt.legend(loc="lower right")
	plt.minorticks_on()
	plt.grid()
	plt.show(block=False)
	plt.savefig(output_name)

def compare_ROC_curves (train_sample, y_train_sample, test_sample, y_test_sample, mva_list, mva_label_list, output_name = 'ComparePerformances.pdf'):
	"""Function that makes a plot of the ROC curve of more than 1
	   MVA at the same time creating a legend. The Legend will contain 
	   the AUC measure and the timing of the MVA application on each of the 
	   processed events.

       input: *)train_sample['array'] = sample on which train the MVA
			  *)y_train_sample['array'] = truth matching on the train sample
			  *)test_sample['array'] = sample on which evaluate the ROC curves
		      *)y_test_sample['array'] = truth matching of the test_sample
		      *)mva_list['array'] = array of objects specifying the mvas
		      *)mva_label_list['array'] = array of labels specifying the labels
										  to put on the legend
			  *)output_name['string'] = name of the output_file
								"""

	plt.figure()
	if (len(mva_list) == 0):
		print 'ERROR: Invalid number of mvas passed to the compare_ROC_curves function'
		print '       Specify the MVAs you want to use in the MVA_list array and the labels yiou want  '
		print '       to use in the MVA_labels array                                                   '
		return 1
	if (len(mva_list) != len(mva_label_list)):
		print 'ERROR: Number of labels different from number of mvas in the compare_ROC_curves function'
		print '       Specify the MVAs you want to use in the MVA_list array and the labels yiou want  '
		print '       to use in the MVA_labels array                                                   '
		return 1
	
	N_processed = len(test_sample)
	
	for k in range (0, len(mva_list)):
		MVA = mva_list[k]
		MVA.fit(train_sample, y_train_sample)
	
		previous_time = time.time()
		test_sample_predict = MVA.predict_proba(test_sample)[:,1]
		new_time = time.time()

		apply_time = new_time - previous_time
		apply_time_event = apply_time / float(N_processed) * 10.e6 #time in microseconds		

		fpr, tpr, thresholds = roc_curve(y_test_sample, test_sample_predict)
			
		roc_auc = auc(fpr, tpr)
		plt.plot(fpr, tpr, lw=1, label=mva_label_list[k]+'(auc = %0.2f) (%0.002f $\mu s /evt.$)' %(roc_auc, apply_time_event))
		plt.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6))
		plt.xlim([-0.05, 1.05])
		plt.ylim([-0.05, 1.05])
		plt.xlabel('False Positive Rate', ha='right', x=1)
		plt.ylabel('True Positive Rate', ha='right', y=1)
		plt.title('Receiver operating characteristic')
		plt.legend(loc="lower right")
		plt.minorticks_on()
		plt.grid()
		plt.show(block=False)
		
	plt.savefig(output_name)

def print_plot (x_axis, y_axis, filename, x_axis_name = '', y_axis_name = '', plotname = ''):

	plt.figure()
	plt.plot(x_axis, y_axis, lw=1)
	#plt.xlim([-0.05, 1.05])
	#plt.ylim([-0.05, 1.05])
	plt.xlabel(x_axis_name, ha='right', x=1)
	plt.ylabel(y_axis_name, ha='right', y=1)
	plt.title(plotname)
	plt.legend(loc="lower right")
	plt.minorticks_on()
	plt.grid()
	plt.show(block=False)
	plt.savefig(filename)


def compare_train_test(clf, X_train, y_train, X_test, y_test, bins=30, filename = 'compare_response.pdf', option = 'decision_function'):
	"""Function that makes the comparison between the training and
       testing data samples. It Works only if the number of classes
	   is 2.

	   	input: *)clf['scikit-learn classifier'] = trained classifier
			   *)X_train['array']   = train sample
               		   *)y_train['array']   = class number of X_train 
									(1 = signal, 0 = background)
			   *)X_test['array']    = test sample
			   *)y_test['array']    = class number of X_test
									(1 = signal, 0 = background)
			   *)bins['int']        = number of bins
			   *)filename['string'] = name of the output file
			   *)option['string']   = function with which compare 										  the responses
									  (decision_function, 										   predict_proba)"""
	

	#Run the classifier. 
	#d1 = signal decision,
	#d2 = background decision 
	plt.figure()
	decisions = []
	for X,y in ((X_train, y_train), (X_test, y_test)):
		if (option == 'decision_function'):
			d1 = clf.decision_function(X[y>0.5]).ravel()
			d2 = clf.decision_function(X[y<0.5]).ravel()
		elif (option == 'predict_proba'):
			d1 = clf.predict_proba(X[y>0.5])[:,1].ravel()
			d2 = clf.predict_proba(X[y<0.5])[:,1].ravel()
		else:
			print 'ERROR in compare_train_test function \n   option '+option+' not known'
			d1 = np.zeros(len(X[y>0.5]))
			d2 = np.zeros(len(X[y>0.5]))
		
		decisions += [d1, d2]
	
	low      = min(np.min(d) for d in decisions)
	high     = max(np.max(d) for d in decisions)
	low_high = (low, high)

	plt.figure(figsize=(8,6))
	plt.hist(decisions[0],color='r', alpha=0.5, range=low_high, bins=bins, histtype='stepfilled', normed=True,label='Signal (train)')
	plt.hist(decisions[1],color='b', alpha=0.5, range=low_high, bins=bins, histtype='stepfilled', normed=True,label='Background (train)')
    
	hist, bins = np.histogram(decisions[2],bins=bins, range=low_high, normed=True)
	scale = len(decisions[2]) / sum(hist)
	err = np.sqrt(hist * scale) / scale
	width = (bins[1] - bins[0])
	center = (bins[:-1] + bins[1:]) / 2
	plt.errorbar(center, hist, yerr=err, fmt='o', c='r', label='Signal (test)')
	hist, bins = np.histogram(decisions[3],bins=bins, range=low_high, normed=True)
	scale = len(decisions[2]) / sum(hist)
	err = np.sqrt(hist * scale) / scale

	plt.errorbar(center, hist, yerr=err, fmt='o', c='b', label='Background (test)')
	plt.xlabel("BDT output", ha='right', x=1)
	plt.ylabel("Arbitrary units", ha='right', y=1)
	plt.legend(loc='best')
	plt.minorticks_on()
	plt.grid()
	plt.show(block=False)
	plt.savefig(filename)

def correlation_matrix (X, y, variables_names, filename='correlation_matrix.pdf'):
	""" Function that produces the correlation matrix for the training variables
	    input: *)X['array'] = train_sample (assuming it is 2d with categories as
							  columns
			   *)y['array'] = truth information on train_sample
			   *)variables_names['array'] = list of variable names
			   *)filename['string'] = name of the output file

	"""
	
	plt.figure()
	#df = pd.DataFrame(np.hstack((X, y.reshape(y.shape[0], -1))), columns=variables_names+['y'])
	df = pd.DataFrame(X, columns=variables_names)

	corrmat = df.corr()


	fig, ax1 = plt.subplots(ncols=1, figsize=(6,5))

	opts = {'cmap': plt.get_cmap("RdBu"),
            'vmin': -1, 'vmax': +1}
    	heatmap1 = ax1.pcolor(corrmat, **opts)
    	plt.colorbar(heatmap1, ax=ax1)

   	ax1.set_title("Correlations")
	labels = corrmat.columns.values
	for ax in (ax1,):
        	# shift location of ticks to center of the bins
        	ax.set_xticks(np.arange(len(labels))+0.5, minor=False)
        	ax.set_yticks(np.arange(len(labels))+0.5, minor=False)
        	ax.set_xticklabels(labels, minor=False, ha='right', rotation=70)
        	ax.set_yticklabels(labels, minor=False)
        
   	plt.tight_layout()
 	plt.savefig(filename)



def control_inputs (sys_argv):
	
	#define the avalaible options for the script
	av_options = ['--NotRecreate', '--MakeROC', '--CompareTrainTest', '--StandardizeData', '--MakePlots', '--ComparePerformances', '--CorrelationMatrix']
	
	for j in range(1, len(sys_argv)):
		if sys_argv[j] == '--options':
			print 'Available options: '
			print '  --NotRecreate'
			print '  --MakeROC    '
			print '  --CompareTrainTest'
			print '  --StandardizeData'
			print '  --MakePlots      '
<<<<<<< HEAD
		    	print '  --ComparePerformances'
=======
		   	print '  --ComparePerformances'
>>>>>>> 68ea6e3541f5ec85e7a081736088e06df3dbf543
			print '  --CorrelationMatrix'
			
			sys.exit()	

	for i in range(1,len(sys_argv)):
		if sys_argv[i] not in av_options:
			print '--Input Error: Option ' + sys_argv[i] + ' not known. \n'
			print 'Available options:'
			print '	 --NotRecreate'
			print '  --MakeROC    '
			print '  --CompareTrainTest'
			print '  --StandardizeData'
			print '  --MakePlots      '
			print '  --ComparePerformances'
			print '  --CorrelationMatrix'
			
			sys.exit()

	
		


