\documentclass{lhcbnote}

\usepackage{listings}

\usepackage{color}
\usepackage{framed}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,           
         % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
  %title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\title{\texttt{SMVA}: Scikit-learn for Multivariate Analysis}
%\author{Benjamin Gaidioz{\address[BGADD]{CERN,Switzerland}},}
%for more than one author 
\author{Simone Meloni{\address[BGADD]{CERN,Switzerland}},
Mark Whitehead{\addressmark[BGADD]},%
Conor Fitzpatrick{\address[NEWADD]{Ecole Polytechnique Federale de Lausanne}}}


\doctyp{Internal Note}
\dociss{1}
\docrev{1}
\docref{LHCb-INT-2016-039}
\doccre{August , 2016}
\docmod{\today}
%\doccnf{You use this command only if you want a note 
%here: like presented at...}

\begin{document}

\maketitle

\begin{abstract}
This note presents a simple python application to perform binary MultiVariate Analysis (MVA) classification with \texttt{scikit-learn} and getting the useful diagnostic output. The tool is standalone and can be modified to utilise different scikit-learn classifiers and with different training variables.
\end{abstract}

\begin{status}
\entry{Draft}{1}{August 12, 2016}{First version}
\entry{Extension}{2}{September 12, 2016}{Added new features to the code}
\end{status}

\tableofcontents

%\listoffigures
%\listoftables

\section{Introduction}
MVA methods are used extensively in data analyses, and in particular binary classification is widely used to reject background from data samples. 
Most of the procedures are based on the \texttt{ROOT TMVA} package, but throughout the Machine Learning community many more techniques and algorithms that are not implemented within this package have been developed. \\

This note describes a simple program written in python that performs binary MVA classification  using the \texttt{scikit-learn} package. This was originally conceived for some Upgrade Trigger studies and is designed to produce TMVA-like output that analysts will be familiar with, while using the additional simplicity and power of \texttt{scikit-learn}.

In the code many of these procedures have been automated and are available thanks to options passed by the user to the shell. A summary of the procedures are as follows:
	\begin{itemize}
	\item ROC curve
	\item Overtraining Check
	\item MVA selection efficiency
	\end{itemize}

The program requires the user to specify the paths of two root files containing the events belonging to each of the classes.
The classifier used by the program can be specified by the user together with an ensemble of selections to apply to the samples.
\\
The minimum output is two root files, saved in  user specified folders, containing respectively the tree of events passing the selection and a tree in which an additional branch, containing the output of the MVA applied to the events passing the selections.  
\\
In the following an example of binary classification made for the signal/background separation in the $D^{+}\rightarrow K^+K^-\pi^+$ channel using \textit{signal} and \textit{minbias} samples from the LHCb upgrade is presented. For this reason the two classes are called \textit{signal} and \textit{background}.

\section{Download}
The latest version of the \texttt{SMVA} program is available on lxplus at:\\
\texttt{/afs/work/s/simeloni/public/SMVA}\\
The requirements are the following:
\begin{itemize}
\item \texttt{ROOT}
\item \texttt{scikit-learn}
\item\texttt{python}
\item\texttt{matplotlib}
\item\texttt{numpy}
\item\texttt{root$\_$numpy}

\end{itemize}

\section{Usage}

The main aim of the program is to train a MVA that performs binary classification and save its output to a new branch of the input root files. The program can be launched using the usual python script launcher


\begin{verbatim}
python SMVA.py
\end{verbatim}


The file \texttt{inputfile.py} contains everything that can be customized by the user. An example of the inputs is shown below:

\lstinputlisting[caption=Example of inputfile.py]{code-files/inputfile.py}

The first variables that can be set are:
\begin{itemize}
\item \texttt{input$\_$sigfilename}
\item \texttt{input$\_$bkgfilename}
\end{itemize}
They have to be set to the relative path in which the root files containing the signal and background sample root files are located. The script will open the root files and will load the Trees named accordingly to the variables:
\begin{itemize}
\item\texttt{input$\_$sigtreename} \item\texttt{input$\_$bkgtreename}
\end{itemize}

The variables 
\begin{itemize}
\item \texttt{output$\_$sigtreename}
\item \texttt{output$\_$bkgtreename}
\end{itemize} contain the name of the trees that will be written in all the output files for each class respectively.
\\

After having loaded the input files, a selection will be applied to the events in each file and the events that pass this selection will be saved in root files in the folders set via:

\begin{itemize}
\item \texttt{output$\_$sigfilename$\_$cuts} \item \texttt{output$\_$bkgfilename$\_$cuts}
\end{itemize}

The saved files are the ones that will contain the events on which the trained MVA will run: the selections applied to create them are stored in the two \texttt{numpy} arrays called:
\begin{itemize}
\item \texttt{sig$\_$preselections}
\item \texttt{bkg$\_$preselections}
\end{itemize}

The code will print informations on the stdout regarding this selection such as the number of events that pass it and the original number of events contained in the file. Other custom variables are used at this stage:
\begin{itemize} 
\item \texttt{N$\_$events$\_$signal} and \texttt{N$\_$events$\_$background}
\item \texttt{sig$\_$truth$\_$matching} and \texttt{bkg$\_$truth$\_$matching}
\end{itemize}

The former is the number of generated events and it will be used to compute the fraction of candidates for each event that pass the selections (called in the Figure \ref{eff-output} \textit{Selected Candidates per Event}) and the latter will be used to compute the selection efficiency with respect to truth matched events (called in the Figure \ref{eff-output} \textit{Selection efficiency with respect to Truth Matched Candidates}). The efficiency called \textit{Selection efficiency} in the Figure \ref{eff-output} is the one computed with respect to the events contained in the input files.\\
\begin{figure}
\begin{framed}
\begin{minipage}{\textwidth}
	\small{
		\begin{verbatim}
[simeloni@pclhcb21 D2KKpi]$ python SMVA.py 
Applying the preliminary selections
===============================================================


INFO: Reducing File                           sample_files/D2KKpi_MD.root
INFO: Saving File after the selections        sample_files/D2KKpi_MD_cuts.root
INFO: Loading Tree                            MCD2kkpiTuple/DecayTree
INFO: Saving Tree after the selections 	      DecayTree
Applying selections...
---------------------------------------------------------------
INFO: Original number of events                         50882
INFO: events in input                                   1788474
INFO: events passing the Preselections                  19473
INFO: events passing Truth Matching                     25515
INFO: events passing Preselections and Truth MAtching   19473


OUTPUT: Selected Candidates per Event        0.38270901301
OUTPUT: Selection Efficieny                  0.0108880531671
OUTPUT: Selection Efficiency with    
        respect to Truth Matched Candidates  0.763198118754
---------------------------------------------------------------
===============================================================
\end{verbatim}
}
\end{minipage}
\end{framed}
\caption{}
\label{eff-output}
\end{figure}

Two other similar numpy arrays are available for the user, and are called:
\begin{itemize} 
\item \texttt{sig$\_$selections$\_$tt}
\item \texttt{bkg$\_$selections$\_$tt}
\end{itemize}
The code will perform this selection on the events contained in the input files and the ensemble of the events that satisfy it will form the sample on which the MVA is trained and tested. The user can decide which is the fraction of this train and test sample that will be dedicated to the training procedure via the \texttt{Train$\_$Ratio} variable. In the example it has been set to 0.5 and it has to be always a positive number strictly less than 1.

After having loaded the training sample, the script will train the MVA that the user has defined in the \texttt{bdt} variable using it. The user has to define the MVA and its hyperparameters and to include the library it belongs to if not already done.
The variables on which the MVA will be trained are defined in the array named \texttt{branches}\\

The last part of the script is dedicated to the saving of the MVA output in a new root file: the events contained in the \texttt{output$\_$sigfilename$\_$cuts} and \texttt{output$\_$bkgfilename$\_$cuts} are processed by the MVA and the new files, whose paths are specified by \texttt{output$\_$sigfilename$\_$cuts$\_$bdt} and \texttt{output$\_$bkgfilename$\_$cuts$\_$bdt}, will contain the classificator output in a branch named \texttt{MVA$\_$branch}.\\

The other variables that have not been described in this section are used only if the user enables some of the options. Their meaning will be made clear in the following chapter.


\section{Options}

The program comes with a list of options that the user can specify to the shell after the name of the program. The sintax is the following:

\begin{verbatim}
python SMVA.py <option_1> <option_2> <option_n>
\end{verbatim}

The aim of the options is to provide the user with automatic tools to assess the MVA performance, to save time during the program execution or to implement useful methods during the MVA training. The program checks the syntax of the options passed to the shell and reminds of the available options if at least one of them is not correct.\\

\begin{figure}
\begin{framed}
\begin{minipage}{\textwidth}
		\begin{verbatim}
		[simeloni@pclhcb21 D2KKpi]$ python SMVA.py --wrong_option
		--Input Error: Option --wrong_option not known. 

		Available options:
            --NotRecreate
            --MakeROC     
            --CompareTrainTest
            --StandardizeData
            --MakePlots   
            --ComparePerformances
            --CorrelationMatrix
\end{verbatim}
\end{minipage}
\end{framed}
\end{figure}

In the following the list of options is given and described.
\subsection{- -options}
This option can be used to print the list  of the available options.

\begin{figure}
\begin{framed}
\begin{minipage}{\textwidth}
		\begin{verbatim}
[simeloni@pclhcb21 D2KKpi]$ python SMVA.py --options
Available options: 
  --NotRecreate
  --MakeROC    
  --CompareTrainTest
  --StandardizeData
  --MakePlots 
  --ComparePerformances
  --CorrelationMatrix
\end{verbatim}
\end{minipage}
\end{framed}
\end{figure}

\subsection{- -NotRecreate}
When this option is given, the creation of a new file with the events that pass the selection given by the \texttt{sig$\_$selections}
and \texttt{bkg$\_$selections} arrays is skipped. In this case the code assumes that the files specified by the variables \texttt{output$\_$sigfilename$\_$cuts}
and \texttt{output$\_$bkgfilename$\_$cuts} already exist and their events will form the ensemble on which the MVA will run.

\begin{figure}
\begin{framed}
\begin{minipage}{\textwidth}
		\begin{verbatim}
[simeloni@pclhcb21 D2KKpi]$ python SMVA.py --NotRecreate 
Not recreating the reduced rate file: input taken from 
--SIGNAL :    ../../sample_files/reduced_rate/D2KKpi_MD_cuts.root
--BACKGROUND: ../../sample_files/reduced_rate/D2KKpi_MB_MD_cuts.root
\end{verbatim}
\end{minipage}
\end{framed}
\end{figure}


\subsection{- -MakeROC}
This option enables the production of the Receiver Operating Charachteristic (ROC). It is a plot that illustrates the performance of a binary classifier as its threshold value is varied. 

The curve is created plotting the True positive rate (i.e. the rate of true signal events passing a selection on the MVA output variable) with respect to the false positive rate (i.e. the rate of background events passing a selection on the MVA output variable).

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{images/ROC.pdf}
\caption{Example of ROC curve created using the \texttt{MakeROC} option}
\end{figure}

The plot is saved in the folder specified by the user via the \texttt{output$\_$plots$\_$folder} variable.

\subsection{- -CompareTrainTest}
Comparing the MVA output distribution for the training and test sample is a very common tool to check overtraining. With this option enabled the program will produce a plot in which the MVA output is compared between the train and test sample for both signal and background. The points belonging to the different classes have different colours and the distributions are normalized.\\

The output plot is saved in the folder specified by the user in the \texttt{output$\_$plots$\_$folder} variable.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{images/compare_response.pdf}
\caption{Example of Overtraining check plot produced using the \texttt{--CompareTrainTest} option}
\end{figure}

\subsection{- -StandardizeData}
Standardization of datasets is a common requirement for many MVA estimators implemented in \texttt{scikit-learn}: it might happen that the classificator behaves badly if the individual features of the class are not normally distributed.

When this option is passed to the shell, the program uses the \texttt{scale} function belonging to the \texttt{preprocessing} class in \texttt{scikit-learn}, to perform the following transformation

	\begin{equation}
		x = \frac{x - \bar{x}}{\sigma_x}
	\end{equation}

After this transformation all the variables have average value 0 and unit variance.
\subsection{- -MakePlots}
The user can enable this option to produce some plots that can be useful to visualize efficiencies and background retention. The signal efficiency here is meant to be the MVA selection efficiency on signal candidates that have passed the preselections.

The plots that are produced are the following:

\begin{itemize}
\item signal efficiency on truth matched candidates (vs) MVA threshold value
\item background retention (vs) MVA threshold
\item signal efficiency  on truth matched candidates (vs) background retention
\end{itemize}

The background retention is evaluated in GB/s and it's computed using the following formula:

\begin{equation}
ret[{\rm GB/s}] = \epsilon_{bkg} \times R_{bkg}[{ \rm MHz}] \times eventsize[{\rm kB}] 
\end{equation}

where $R_{bkg}$ is the expected background rate, provided by the user in the \texttt{background$\_$rate} variable, and eventsize is the expected size for each event, provided by the user in the \texttt{event$\_$size} variable, and $\epsilon_{bkg}$ is the fraction of selected background candidates per event.

\subsection{- -ComparePerformances}
When this option is passed to the script, the program produces a plot in which more than one MVA's ROC curve is shown. The MVAs used are the ones specified in the \texttt{MVAS$\_$list} variable. The user must also specify the labels he wants to use in the legend using the variable \texttt{MVA$\_$labels}\\

The program checks if the lenght of the two arrays is the same and if it's not the case prints an error message.\\

\begin{figure}
\centering
\includegraphics[scale=0.5]{images/compare_performances.pdf}
\caption{Example of plot produced enabling the \texttt{--ComparePerformances} option}
\label{fig_compare_mvas}
\end{figure}

In Figure \ref{fig_compare_mvas} there is an example of the output plot associated with this option: on the side of the legend lables it is reported the area under the curve (auc) for each MVA and the time needed by the MVA to run on each event expressed in $\mu s /{\rm{evt.}}$
 
\subsection{- -CorrelationMatrix}
Using this option the user is able to produce the correlation matrix for the distributions of the variables with which the script is training the MVA. An example is given in figure \ref{fig_correlation} The correlations computed are the usual ones, given in equation \ref{eq_correlation} and are computed on the sample of candidates that pass the preselections.

\begin{equation}
\rm{corr}(X,Y) = \frac{\rm{E}[(X-\mu_X)(Y-\mu_Y)]}{\sigma_X \sigma_Y}
%\rm{E}\left[(X-\mu_X)(Y-\mu_Y)\right]}{\sigma_X \sigma_Y}
\label{eq_correlation}
\end{equation}

\begin{figure}
\centering
\includegraphics[scale=0.7]{images/correlation_matrix.pdf}
\caption{Example of plot produced enabling the \texttt{--CorrelationMatrix} option}
\label{fig_correlation}
\end{figure}



\section{Conclusions}
With this simple script most of the plots usually produced with the \texttt{TMVA} package have been automated using the \texttt{scikit-learn} package. Corrections, suggestions, bug reports and feature requests are very much appreciated and the code is available for modification.

\section{Acknowledgements}
The author wishes to thank Mark Whitehead and Conor Fitzpatrick at CERN: this work has been conducted during the Summer Student internship and it would not have been possible without their support and their inputs.  An invaluable help has been kindly offered by Igor Babuschkin from Manchester University: without him it would not have been possible to install all the necessary packages and to set up the environment. Finally a big aknowledgement is deserved to Tim Head \cite{Scikitlearn}, whose blog both inspired this work and provided the most of the procedures it uses.

%\section{References}

\begin{thebibliography}{1}
\bibitem{ROOT} Renè Brun and Fons Rademakers. ROOT: A data Analysis Framework http://root.cern.ch/
\bibitem{Scikitlearn} Tim Head's Blog : http://betatim.github.io/
\end{thebibliography}

\end{document}

