\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Download}{2}
\contentsline {section}{\numberline {3}Usage}{2}
\contentsline {section}{\numberline {4}Options}{7}
\contentsline {subsection}{\numberline {4.1}- -options}{7}
\contentsline {subsection}{\numberline {4.2}- -NotRecreate}{8}
\contentsline {subsection}{\numberline {4.3}- -MakeROC}{8}
\contentsline {subsection}{\numberline {4.4}- -CompareTrainTest}{9}
\contentsline {subsection}{\numberline {4.5}- -StandardizeData}{9}
\contentsline {subsection}{\numberline {4.6}- -MakePlots}{9}
\contentsline {subsection}{\numberline {4.7}- -ComparePerformances}{10}
\contentsline {subsection}{\numberline {4.8}- -CorrelationMatrix}{10}
\contentsline {section}{\numberline {5}Conclusions}{11}
\contentsline {section}{\numberline {6}Acknowledgements}{11}
\contentsline {section}{\numberline {7}References}{11}
