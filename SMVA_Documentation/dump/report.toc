\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Download}{2}
\contentsline {section}{\numberline {3}Usage}{2}
\contentsline {section}{\numberline {4}Options}{6}
\contentsline {subsection}{\numberline {4.1}- -options}{7}
\contentsline {subsection}{\numberline {4.2}- -NotRecreate}{7}
\contentsline {subsection}{\numberline {4.3}- -MakeROC}{7}
\contentsline {subsection}{\numberline {4.4}- -CompareTrainAndTest}{8}
\contentsline {subsection}{\numberline {4.5}- -StandardizeData}{8}
\contentsline {subsection}{\numberline {4.6}- -MakePlots}{9}
\contentsline {section}{\numberline {5}Conclusion}{9}
\contentsline {section}{\numberline {6}Acknowledgements}{9}
\contentsline {section}{\numberline {7}References}{9}
