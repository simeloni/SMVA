import numpy as np
from array import array

from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression, SGDClassifier, Perceptron, PassiveAggressiveClassifier
from sklearn.ensemble import AdaBoostClassifier


###############################################################################
#			    MODIFY					      #	
###############################################################################

#-----------Location and name of the input files and the output files---------#
input_sigfilename = '../../sample_files/D2KKpi_MD.root'
input_bkgfilename = '../../sample_files/D2KKpi_MB_MD.root'

input_sigtreename   = 'MCD2kkpiTuple/DecayTree'
input_bkgtreename   = 'MCD2kkpiTuple/DecayTree'

output_sigtreename  = 'DecayTree'
output_bkgtreename  = 'DecayTree'

#output file after selections
output_sigfilename_cuts = '../../sample_files/reduced_rate/D2KKpi_MD_cuts.root'
output_bkgfilename_cuts = '../../sample_files/reduced_rate/D2KKpi_MB_MD_cuts.root'

#output file after selections and bdt application
output_sigfilename_cuts_bdt = '../../sample_files/output_files/D2KKpi_MB_MD_cuts_bdt.root'
output_bkgfilename_cuts_bdt = '../../sample_files/output_files/D2KKpi_MB_MD_cuts_bdt.root'

#-------------------------------MVA Branch Name--------------------------------#
mva_branch_name = 'MVA_D2KKpi'

#----------------------------------Plots folder -------------------------------#
output_plots_folder = 'Plots/'

#------------------------Define the Background rate [MHz]----------------------#
# This variable can be used to evaluate the background retention after the 
# preselection and after the various MVA selection scenarios for an 
# hypothetical trigger. In particular it 
# will be used if the option --MakePlots has been given.

background_rate = 30.

#--------------------------------Define the event size [KB/evt]----------------#
##This variable can be used to evaluate the background retention after the 
# preselection and after the various MVA selection scenarios for an 
# hypothetical trigger. In particular it 
# will be used if the option --MakePlots has been given.

event_size = 14.


#-------------------------------Define the number of events--------------------#
# This must be the number of generator level events before the reconstruction.
# This variable is used to evaluate the fraction of candidates per event
# that pass the selections


N_events_signal     = 50882
N_events_background = 938866

#-----------------Define the MVA that will be used-----------------------------#
# See the documentation at http://scikit-learn/stable for more informations 
# about the classifiers and their hyperparameters 

#bdt = DecisionTreeClassifier()
#bdt = RandomForestClassifier()
#bdt = GaussianNB()
#bdt = LinearDiscriminantAnalysis()
#bdt = LogisticRegression()_
bdt = AdaBoostClassifier()

#-----------Apply the proper cuts for the train & test sample------------------#

#selections for the train and test sample
sig_preselections_tt = np.array([  '(cand_PT > 2000)',
				'(cand_LOKI_BPVLTIME>0.0002)', 
				'(dau1_PT > 250)',
				'(dau2_PT > 250)',
				'(dau3_PT > 250)',
				'(cand_LOKI_VFASPF_VCHI2VDOF < 25 )',
				'(dau1_TRACK_CHI2NDOF < 6.0)',
				'(dau2_TRACK_CHI2NDOF < 6.0)',
				'(dau3_TRACK_CHI2NDOF < 6.0)',
				'(dau1_TRACK_GhostProb<0.4)', 
				'(dau2_TRACK_GhostProb<0.4)',
			  	'(dau3_TRACK_GhostProb<0.4)',
				'((cand_BKGCAT == 0) || (cand_BKGCAT == 10))'])

bkg_preselections_tt = np.array([  '(cand_PT > 2000)',
				'(cand_LOKI_BPVLTIME>0.0002)', 
				'(dau1_PT > 250)',
				'(dau2_PT > 250)',
				'(dau3_PT > 250)',
				'(cand_LOKI_VFASPF_VCHI2VDOF < 25 )',
				'(dau1_TRACK_CHI2NDOF < 6.0)',
				'(dau2_TRACK_CHI2NDOF < 6.0)',
				'(dau3_TRACK_CHI2NDOF < 6.0)',
				'(dau1_TRACK_GhostProb<0.4)', 
				'(dau2_TRACK_GhostProb<0.4)',
				'(dau3_TRACK_GhostProb<0.4)',
				'(cand_BKGCAT > 35)'])

#selection for the apply sample
sig_preselections = np.array([     '(cand_PT > 2000)',
				'(cand_LOKI_BPVLTIME>0.0002)', 
				'(dau1_PT > 250)',
				'(dau2_PT > 250)',
				'(dau3_PT > 250)',
				'(cand_LOKI_VFASPF_VCHI2VDOF < 25 )',
				'(dau1_TRACK_CHI2NDOF < 6.0)',
				'(dau2_TRACK_CHI2NDOF < 6.0)',
				'(dau3_TRACK_CHI2NDOF < 6.0)',
				'(dau1_TRACK_GhostProb<0.4)', 
				'(dau2_TRACK_GhostProb<0.4)',
				'(dau3_TRACK_GhostProb<0.4)',
				'((cand_BKGCAT == 0) || (cand_BKGCAT == 10))'])
				
bkg_preselections = np.array([	'(cand_PT > 2000)',
				'(cand_LOKI_BPVLTIME>0.0002)', 
				'(dau1_PT > 250)',
				'(dau2_PT > 250)',
				'(dau3_PT > 250)',
				'(cand_LOKI_VFASPF_VCHI2VDOF < 25 )',
				'(dau1_TRACK_CHI2NDOF < 6.0)',
				'(dau2_TRACK_CHI2NDOF < 6.0)',
				'(dau3_TRACK_CHI2NDOF < 6.0)',
				'(dau1_TRACK_GhostProb<0.4)', 
				'(dau2_TRACK_GhostProb<0.4)',
				'(dau3_TRACK_GhostProb<0.4)'])
						   								   
#---------------define the truth matching selection---------------------------#
sig_truth_matching = '((cand_BKGCAT == 0) || (cand_BKGCAT == 10) )'
bkg_truth_matching = '(cand_BKGCAT > 35)'

#-------------- define the Train Ratio----------------------------------------#
#define the fraction of events contained in the train and test sample that will be
#used for training the MVA. It must be a float greater than 0 and strictly less than 1.

Train_Ratio = 0.5


#--------------define the list of variables used to train the MVA--------------#
branches = [	'dau1_PT'                    ,
		'dau2_PT'                    ,
		'dau3_PT'                    ,
		'dau1_LOKI_MIPCHI2DV_PRIMARY',
	        'dau2_LOKI_MIPCHI2DV_PRIMARY',
	        'dau3_LOKI_MIPCHI2DV_PRIMARY',
	        'cand_LOKI_VFASPF_VCHI2VDOF' ,
	        'cand_LOKI_BPVDIRA'          ,  
	        'cand_PT'                    ,
	        'dau1_PIDK'                  ,
	        'dau2_PIDK'                  ,
	        'dau3_PIDK'                  ]

#------------define the list of MVAs and the list of labels if you want--------#
#------------to use the --ComparePerformances option---------------------------#

MVAS_list = [  DecisionTreeClassifier(max_depth=3)                ,
               RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1)                                                   ,
               AdaBoostClassifier()                               ,
               GaussianNB()                                       ,
               LinearDiscriminantAnalysis()                       ,
               QuadraticDiscriminantAnalysis()                    ,
			   LogisticRegression()   ]

MVAS_labels = ['Decision Tree'                  ,
         	   'Random Forest'                  ,
		 	   'AdaBoost'                       ,
		  	   'Naive Bayes'                    ,
		 	   'Linear Discr.'            ,
               'Quadratic Discr.'         ,
		       'Logistic Regr.']



###############################################################################

###############################################################################

